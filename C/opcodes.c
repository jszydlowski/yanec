// opcodes

#include "opcodes.h"
#include "nes.h"

uint16 make_ptr16(uint8 lo, uint8 hi)
{
	uint16 result = hi * 0x100 + lo;
	printf("$%02x, $%02x = $%04x\n", lo, hi, result);
	return result;
}

uint8 handle_opcode(uint8 opcode, uint8 a1, uint8 a2)
{
	printf("handle opcode:--------------\n");
	uint8 result = 0;

	switch (opcode)
	{

	// JMP
	case 0x4C: // JMP ABSOLUTE
	{
		PC = make_ptr16(a1, a2);
		break;
	}
	case 0x6C: // JMP INDIRECT
	{
		uint8 lo = memory_get(make_ptr16(a1, a2));
		uint8 hi = memory_get(make_ptr16(a1, a2)+1);
		PC = make_ptr16(lo, hi);
		break;
	}

	
	// LDA LoaD Accumulator
	case 0xA9: // LDA IMMEDIATE
	{
		A = a1;
		PC += 2;
		p_set_negative(A);
		break;
	}
	case 0xA5: // LDA ZERO PAGE
	{
		A = memory_get(get_zero_page_address(a1));
		p_set_negative(A);
		PC += 2;
		break;
	}
	case 0xB5: // LDA ZERO PAGE,X
	{
		A = memory_get(get_zero_page_address((uint8)(a1 + X)));
		p_set_negative(A);
		PC += 2;
		break;
	}
	case 0xAD: // LDA ABSOLUTE
	{
		A = memory_get(make_ptr16(a1, a2));
		p_set_negative(A);
		PC += 3;
		break;
	}
	case 0xBD: // LDA ABSOLUTE,X
	{
		A = memory_get((uint16)(make_ptr16(a1, a2)+X));
		p_set_negative(A);
		PC += 3;
		break;
	}
	case 0xB9: // LDA ABSOLUTE,Y
	{
		A = memory_get((uint16)(make_ptr16(a1, a2)+Y));
		p_set_negative(A);
		PC += 3;
		break;
	}


	// LDX
	case 0xA2: // LDX IMMEDIATE
	{
		X = a1;
		p_set_negative(X);
		PC += 2;	
		break;
	}
	case 0xA6: // LDX ZERO PAGE
	{
		X = memory_get(get_zero_page_address(a1));
		p_set_negative(X);
		PC += 2;
		break;
	}
	case 0xB6: // LDX ZARO PAGE X
	{
		uint8 result_offset = (uint8)(a1 + X);
		X = memory_get(get_zero_page_address(result_offset));
		p_set_negative(X);
		PC += 2;
		break;
	}
	case 0xAE: // LDX ABSOLUTE
	{
		X = memory_get(make_ptr16(a1, a2));
		p_set_negative(X);
		PC += 3;
		break;
	}
	case 0xBE: // LDX ABSOLUTE, X
	{
		X = memory_get((uint16)make_ptr16(a1, a2) + X);
		p_set_negative(X);
		PC += 3;
		break;
	}


	// STX
	case 0x86: // STX ZERO PAGE
	{
		memory_set(get_zero_page_address(a1), X);
		PC += 2;
		break;
	}
	case 0x96: // STX ZERO PAGE Y
	{
		memory_set((uint16)(get_zero_page_address(a1) + Y), X);
		PC += 2;
		break;
	}
	case 0x8E: // STX ABSOLUTE
	{
		memory_set(make_ptr16(a1, a2), X);
		PC += 3;
		break;
	}


	// JSR - Jump SubRoutine
	case 0x20: // JSR ABSOLUTE
	{
		stack_push(get_ptr_hi(PC+2));
		stack_push(get_ptr_lo(PC+2));
		PC = make_ptr16(a1, a2);
		break;
	}

	// RTS ReTurn from Subroutine
	case 0x60: // RST
	{
		uint8 lo = stack_pop();
		uint8 hi = stack_pop();
		PC = make_ptr16(lo, hi) + 1;
		break;
	}


	// NOP No OPeration
	case 0xEA: // NOP
	{
		PC += 1;
		break;
	}


	// PROCESSOR FLAG OPERATIONS
	case 0x18: // CLC CLear Carry flag
	{
		p_set_carry(0);
		PC += 1;
		break;
	}
	case 0x38: // SEC SEt Carry flag
	{
		p_set_carry(1);
		PC += 1;
		break;
	}



	// BRANCH INSTRUCTIONS
	case 0x10: // BPL Branch on PLus 
	{
		if(!p_get(P_N))
		{
			PC += a1; /* Instruction offset */
		}
		PC += 2;
		break;
	}
	case 0x30: // BMI Branch on MInus
	{
		if(p_get(P_N))
		{
			PC += a1; /* Instruction offset */
		}
		PC += 2;
		break;
	}
	case 0x50: // BVC Branch on oVerflow Clear
	{
		if(!p_get(P_V))
		{
			PC += a1; /* Instruction offset */
		}
		PC += 2;
		break;
	}
	case 0x70: // BCS Branch on oVerflow Set
	{
		if(p_get(P_V))
		{
			PC += a1; /* Instruction offset */
		}
		PC += 2;
		break;
	}
	case 0x90: // BCC Branch on Carry Clear
	{
		if(!p_get(P_C))
		{
			PC += a1; /* Instruction offset */
		}
		PC += 2;
		break;
	}
	case 0xB0: // BCS Branch on Carry Set
	{
		if(p_get(P_C))
		{
			PC += a1; /* Instruction offset */
		}
		PC += 2;
		break;
	}
	case 0xD0: // BNE Branch on Not Equal
	{
		if(p_get(P_ZERO))
		{
			PC += a1; /* Instruction offset */
		}
		PC += 2;
		break;
	}
	case 0xF0: // BEQ Branch on EQual
	{
		if(!p_get(P_ZERO))
		{
			PC += a1; /* Instruction offset */
		}
		PC += 2;
		break;
	}
	

	// STACK INSTRUCTIONS
	case 0x9A: // TXS
	{
		SP = X;
		PC += 1;
		break;
	}
	case 0xBA: // TSX
	{
		X = SP;
		PC += 1;
		break;
	}
	case 0x48: // PHA
	{
		stack_push(A);
		PC += 1;
		break;
	}
	case 0x68: // PLA
	{
		A = stack_pop();
		PC += 1;
		break;
	}
	case 0x08: //PHP
	{
		stack_push(P);
		PC += 1;
		break;
	}
	case 0x28: // PLP
	{
		P = stack_pop();
		PC += 1;
		break;
	}


	// REGISTER INSTRUCTIONS
	case 0xAA: // TAX
	{
		X = A;
		PC += 1;
		break;
	}
	case 0x8A: // TXA
	{
		A = X;
		PC += 1;
		break;
	}
	case 0xCA: // DEX
	{
		X--;
		PC += 1;
		break;
	}
	case 0xE8: // INX
	{
		X++;
		PC += 1;
		break;
	}
	case 0xA8: // TAY
	{
		Y = A;
		PC += 1;
		break;
	}
	case 0x98: // TYA
	{
		A = Y;
		PC += 1;
		break;
	}
	case 0x88: // DEY
	{
		Y--;
		PC += 1;
		break;
	}
	case 0xC8: // INY
	{
		Y++;
		PC += 1;
		break;
	}

	// ORA
    	case 0x09: // ORA immediate
	{

		PC += 2;
		break;
	}

	}

	printf("handle opcode:--------------\n");
	return result;
}

