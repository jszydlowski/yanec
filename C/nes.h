/*
 *
 *
 */

#include "types.h"

#define CPU_MEMORY_SIZE 2048
#define PPU_MEMORY_SIZE 16384
#define CPU_STACK_OFFSET 0x0100
#define PRGROM_OFFSET 0x8000

// HARDWARE
// Memory
uint8 cpu_memory[CPU_MEMORY_SIZE];
uint8 ppu_memory[PPU_MEMORY_SIZE];

// Registers
extern uint16  PC; // Program Counter
extern uint8   SP; // Stack Pointer
extern uint8   A;  // Accumulator
extern uint8   X;  // X register
extern uint8   Y;  // Y register
extern uint8   P;  // Processor status

// Status register flags
#define P_C 0x01 << 0
#define P_Z 0x01 << 1
#define P_I 0x01 << 2
#define P_D 0x01 << 3
#define P_B 0x01 << 4
#define P_V 0x01 << 6
#define P_N 0x01 << 7

#define P_CARRY 	P_C
#define P_ZERO		P_Z
#define P_INTERUPT	P_I
#define P_DECIMAL	P_D
#define P_BREAK		P_B
#define P_OVERFLOW	P_V
#define P_NEGATIVE	P_N

// static unsigned long int cpu_cycles_from_restart = 0;

void nes_info();
void debug_print_P();
void debug_print_stack();
void debug_singleline_cpu_dump();

void memory_set(uint16 adress, uint8 value);
uint8 memory_get(uint16 adress);

uint8 get_bit(uint8 value, uint8 bit);
uint8 set_bit(uint8 source, uint8 bit, uint8 value);

uint8 get_ptr_lo(uint16 address);
uint8 get_ptr_hi(uint16 address);

void stack_push(uint8 value);
uint8 stack_pop();

uint8 check_p(uint8 flag);

uint16 get_physical_address(uint16 adress);

uint16 get_zero_page_address(uint8 address); 

uint8 p_get(uint8 flag);
void p_set_negative(uint8 operand);
void p_set_carry(uint8 operand);

void nes_restart();

void debug_print_cpu_info();

