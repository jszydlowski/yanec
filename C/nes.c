#include <stdio.h>
#include "nes.h"
#include "rom.h"

#include "opcodes.h"

//uint8 handle_opcode(uint8, uint8, uint8);

uint16  PC	= 0x0000;	// Program Counter
uint8   SP	=	0xFF;	// Stack Pointer
uint8   A	=	0x00;	// Accumulator
uint8   X	=	0x00;	// X register
uint8   Y	=	0x00;	// Y register
uint8   P	=	0x00;	// Processor status



void nes_info()
{
	printf("nes_info call\n");
	debug_singleline_cpu_dump();

    P = P_C | P_I | P_V;
	debug_print_P();
	debug_singleline_cpu_dump();

	stack_push(5);
	stack_push(15);
	stack_push(25);

	stack_pop();
	stack_pop();

	for (int x = 0; x < 512; ++x)
	{
		stack_push( x % 256 );
	}

	debug_print_stack();

	handle_opcode(0, 0, 0);
	debug_print_P();

}


void debug_singleline_cpu_dump()
{
	//      PC
	printf("%04x  %02x %02x %02x  ___ _____                       A:%02x X:%02x Y:%02x\n",
		   PC,
		   memory_get(PC), memory_get(PC+1), memory_get(PC+2),
		   A, X, Y);
}


uint8 get_bit(uint8 value, uint8 bit)
{
	return (value >> bit) & 1;
}

uint8 set_bit(uint8 source, uint8 bit, uint8 value)
{
	source ^= (-value ^ source) & (1 << bit);
	return source;
}


uint16 get_physical_address(uint16 adress)
{
	uint16 result = adress;
	
	if( (result >= 0x0000) && (result < 0x2000) )
	{
		result = result % 0x0800;
	}
	else if ( (result >= 0x2000) && (result < 0x4000) )
	{
		result -= 0x2000;
		result = result % 8;
		result += 0x2000;
	}
	return result;
}


uint16 get_zero_page_address(uint8 address) 
{
	return (uint16)0x0000 + address;
}

uint8 get_ptr_lo(uint16 address)
{
	return address & 0x00FF;
}

uint8 get_ptr_hi(uint16 address)
{
	return address >> 8;
}

uint8 p_get(uint8 flag)
{
	return (P & flag) & 0x1; 
}

void p_set_negative(uint8 operand)
{
//	P ^= (-((operand >> P_N) & 1) ^ P) & (P_N);
	P = set_bit( P, P_N, get_bit(operand, P_N));
}

void p_set_carry(uint8 operand)
{
	P = set_bit(P, P_C, operand);
}

void memory_set(uint16 adress, uint8 value)
{
	uint16 phy_adress = get_physical_address(adress);
	cpu_memory[phy_adress] = value;
}

uint8 memory_get(uint16 adress)
{
	uint16 phy_adress = get_physical_address(adress);
	uint8 result = 0;

//	result = cpu_memory[phy_adress];

	if(phy_adress >= PRGROM_OFFSET)
	{
//		result = prg_rom[ adress - PRGROM_OFFSET ];
	}

	return result;
}


void debug_print_P()
{
    printf("DEBUG: Processor Status Flags:----------------\n");
//    printf("76543210\n");
    printf("NV_BDIZC\n");
    printf("%d%d_%d%d%d%d%d\n",
           check_p(P_N), check_p(P_V), check_p(P_B),
           check_p(P_D), check_p(P_I), check_p(P_Z),
           check_p(P_C));
    printf("DEBUG: Processor Status Flags:----------------\n");
}

uint8 check_p(uint8 flag)
{
    return (P & flag) && 0x1;
}

void stack_push(uint8 value)
{
	cpu_memory[CPU_STACK_OFFSET + SP] = value;
	SP--;
}

uint8 stack_pop()
{
	SP++;
	uint8 value = cpu_memory[CPU_STACK_OFFSET + SP];
	return value;
}

void nes_restart()
{
	PC = 0x0;
	SP = 0xFF; // Stack is top-down
	A = 0x0;
	X = 0x0;
	Y = 0x0;
	P = 0x0;
}

void debug_print_stack()
{
	printf("DEBUG: Stack Dump:--------------------------\n");
    printf("SP=$%04x\n", SP);
    for (int j = 0; j < 16; ++j)
	{
        printf("$%04x: ", CPU_STACK_OFFSET + j * 16);
        for (int i = 0; i < 16; ++i)
        {
            if( SP == j*16+i )
            {
                printf("[");
            }
            else
            {
                printf(" ");
            }
            printf("%02x", cpu_memory[CPU_STACK_OFFSET + (j * 16) + i]);
            if( SP == j*16+i )
            {
                printf("]");
            }
            else
            {
                printf(" ");
            }
        }
		printf("\n");
	}
	printf("DEBUG: Stack Dump:--------------------------\n");
}

void debug_print_cpu_info()
{
	printf("A:%02u X:%02u Y:%02u P:%02u SP:%02u CYC:--\n", A, X, Y, PC, SP);
}

