# yaNEC #
### yet another NES Emulator Core ###
by *Jacek "J_SEC" Szydlowski*

## Project scope ##
### Phase 1 ###
* Create basic working NES emulator in C
* Have fun

### Phase 2 ###
* Fix bugs
* Port it to different languages
* Port it to embedded device