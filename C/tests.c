#include <stdio.h>

#include "types.h"
#include "nes.h"
#include "rom.h"
#include "opcodes.h"

// Physical adress mapping 
// get_physical_address

uint8 check_uint16(uint16 a, uint16 b)
{
	int result = !(a == b);
	if (result)
	{
        //TODO: BUG: pritnf gets wrong data (whole 8 bytes not 4 as needed)
        printf("CHECK UINT16: ERROR\t%d($%04x) != %d($%04x)\n", a, a, b, b);
	}

	return result;
}

int test_get_physical_address()
{
	int errors = 0;

    // CPU MEMORY MIRRORING
    errors += check_uint16(0x0100, get_physical_address( 0x0100 ));
    errors += check_uint16(0x0000, get_physical_address( 0x0800 ));
    errors += check_uint16(0x0423, get_physical_address( 0x1423 ));
    errors += check_uint16(0x0423, get_physical_address( 0x1C23 ));

    // IO MIRRORING
    errors += check_uint16(0x2004, get_physical_address( 0x2004 ));
    errors += check_uint16(0x2000, get_physical_address( 0x2008 ));
    errors += check_uint16(0x2001, get_physical_address( 0x2009 ));

	printf("test_get_physical_address() = %d\n", errors);
	return errors;
}

int test_rom_load()
{
	int errors = 0;

	rom_load_file("a.nes");
	rom_load_file("b.zip");
	rom_load_file("nestest.nes");

//    assertion test
//    rom_load_file(0);

	printf("test_rom_load() = %d\n", errors);
	return errors;
}

int test_make_ptr16()
{
	make_ptr16(0xC0, 0x10);
	make_ptr16(0x01, 0x10);
	make_ptr16(0xaa, 0xbb);
	make_ptr16(0xf5, 0xc5);
	return 0;
}

int test_get_ptr_lo_hi()
{
	int errors = 0;
	if(get_ptr_lo(0x1234) != 0x34) { errors++; printf("get_ptr_lo(0x1234) = %02u\n", get_ptr_lo(0x1234));} 
	if(get_ptr_hi(0x1234) != 0x12) { errors++; printf("get_ptr_hi(0x1234) = %02u\n", get_ptr_lo(0x1234));} 

	printf("lo(0xabcd) = $%02x\n", get_ptr_lo(0xabcd));
	printf("hi(0xabcd) = $%02x\n", get_ptr_hi(0xabcd));

	printf("test_get_ptr_lo_hi() = %d\n", errors); 
	return errors;
}

int run_tests()
{
	int errors = 0;
	errors += test_get_physical_address();
	errors += test_rom_load();
	errors += test_make_ptr16();
	errors += test_get_ptr_lo_hi();
		
	return errors;
}


int main(int argc, char* argv[])
{
	printf("Test procedure start:\n");
	printf("Numer of errors = %d\n", run_tests() );
	printf("Test procedure end:\n");
}

