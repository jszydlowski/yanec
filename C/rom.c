#include <assert.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <ctype.h>

#include "rom.h"

uint8		rom_status = ROM_NOT_LOADED;
RomHeader*	header_ptr;
uint8*      prg_rom_array = 0;
uint8*      chr_rom_array = 0;

RomStatus rom_load_file(char* filename)
{
    assert(filename);

	RomStatus result = ROM_NOT_LOADED;

	header_ptr = (RomHeader*)malloc( sizeof( RomHeader ) );
    header_ptr->rom_type = ROM_TYPE_INVALID;
    header_ptr->rom_banks = 0;
    header_ptr->vrom_banks = 0;

	// check rom type
	size_t filename_length = strlen(filename);

	if( filename[filename_length-4] == '.' &&
		tolower(filename[filename_length-3]) == 'n' &&
		tolower(filename[filename_length-2]) == 'e' &&
		tolower(filename[filename_length-1]) == 's' )
	{
		printf("iNES ROM EXTENSION\n");
        uint8 ines_header[INES_HEADER_SIZE];

        FILE* file_ptr = 0;
        file_ptr = fopen(filename, "rb");

        if(!file_ptr)
        {
            printf("ERROR: FILE NOT OPPENED\n");
            return ROM_ERROR_FILE_NOT_OPENED;
        }

        // Load header
        fread(ines_header, INES_HEADER_SIZE, 1, file_ptr);
        printf("LOADED HEADER:");
        for( int i = 0; i < INES_HEADER_SIZE; ++i )
        {
            printf(" %02x", ines_header[i]);
        }

        if( (ines_header[0] == 0x4E) &&
            (ines_header[1] == 0x45) &&
            (ines_header[2] == 0x53) &&
            (ines_header[3] == 0x1A) )
        {
            header_ptr->rom_type = ROM_TYPE_INES;
            header_ptr->rom_banks = ines_header[4];
            header_ptr->vrom_banks = ines_header[5];
            printf("\nPRG:%d, CHR:%d\n", header_ptr->rom_banks, header_ptr->vrom_banks);

            // allocate memory for roms
            prg_rom_array = malloc(PRG_ROM_BANK_SIZE * header_ptr->rom_banks);
            chr_rom_array = malloc(CHR_ROM_BANK_SIZE * header_ptr->vrom_banks);

            fread(prg_rom_array, PRG_ROM_BANK_SIZE, header_ptr->rom_banks, file_ptr);
            fread(chr_rom_array, CHR_ROM_BANK_SIZE, header_ptr->vrom_banks, file_ptr);

            printf("Allocated memory adressess: PRG:%p, CHR:%p\n", prg_rom_array, chr_rom_array);
            printf("PRG 0,1 Byte = %02x %02x\n", prg_rom_array[0], prg_rom_array[1]);
            printf("CHR 0,1 Byte = %02x %02x\n", chr_rom_array[0], chr_rom_array[1]);
        }




	}

	return result;
}

void rom_free_rom()
{
	if( header_ptr )
	{
		free(header_ptr);
		header_ptr = 0;
		rom_status = ROM_NOT_LOADED;
	}
}
