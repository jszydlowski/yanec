#ifndef ROM_H
#define ROM_H

#include "types.h"

#define ROM_TYPE_INVALID                0
#define ROM_TYPE_INES                   100
#define ROM_TYPE_UNIF                   200

#define ROM_LOADED                      0
#define ROM_NOT_LOADED                  1
#define ROM_ERROR_FILE_NOT_OPENED       2

#define INES_HEADER_SIZE 16
#define PRG_ROM_BANK_SIZE 8192
#define CHR_ROM_BANK_SIZE 16384

typedef uint8 RomStatus;

typedef struct RomHeader
{
    uint8 rom_type;
	uint8 rom_banks;
	uint8 vrom_banks;
} RomHeader;

extern uint8		rom_status;
extern RomHeader*	header_ptr;
extern uint8*       prg_rom;
extern uint8*       chr_rom;

RomStatus rom_load_file(char* filename);
void rom_free_rom();

#endif // ROM_H
