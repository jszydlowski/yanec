#include <stdio.h>
#include "types.h"


uint16 make_ptr16(uint8 lo, uint8 hi);
uint8 handle_opcode(uint8 opcode, uint8 a1, uint8 a2);

